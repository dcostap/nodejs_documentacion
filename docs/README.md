# Creating a custom Java Swing Button

## Usage example

Before explaining how it was done, let's take a look at how the finished custom button works:

![Usage GIF](./media/usage1.gif)

As we can see, the button has custom colors that change based on whether we are hovering over it or not.

We can edit those colors (differentiating between text and background) directly in the NetBeans UI:

![Usage GIF](./media/usage2.gif)

## First step: Creating our custom property

To allow the modification of background & text colors on both `hover` and `non-hover` states, we need to first create the property which will store both colors. We'll later instantiate 2 properties, one for each button state.

```java
public class ColorProperty implements Serializable {
    private Color textColor;
    private Color backgroundColor;

    public ColorProperty() {
        this.textColor = Color.BLACK;
        this.backgroundColor = Color.WHITE;
    }

    public ColorProperty(Color textColor, Color backgroundColor) {
        this.textColor = textColor;
        this.backgroundColor = backgroundColor;
    }

    public Color getTextColor() {
        return textColor;
    }

    public void setTextColor(Color textColor) {
        this.textColor = textColor;
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }
}
```

## Second step: Extending the Button class

Since we only want to add the capability to change colors, we reuse all the behavior already coded in the default JavaSwing's Button. We'll just add the two variables instantiating the custom property we just created.

Our new class must be a JavaBean, so it must implement Serializable and provide a default no-arguments constructor.

## Third step: Custom JPanel for the UI editor

![Tutorial PNG](./media/tut1.png)

We create and design the JPanel that will be used when we edit the custom property we created.

This new JPanel must provide a method that returns the new property:

```java
    public ColorProperty getSelectedValue() {
        return new ColorProperty(textColor, backColor);
    }
```

Next we link the panel with our custom property editor. We do so by subclassing `PropertyEditorSupport` and overriding the relevant methods:

```java
public class ColorPropertyEditorSupport extends PropertyEditorSupport {

    private ColorPropertyPanel cpp = new ColorPropertyPanel();

    @Override
    public boolean supportsCustomEditor() {
        return true;
    }

    @Override
    public Component getCustomEditor() {
        return cpp;
    }

    @Override
    public String getJavaInitializationString() {
        ColorProperty cp = cpp.getSelectedValue();
        return "new com.dariocosta.exercisebeans.ColorProperty" +
                "(" +
                "new java.awt.Color("
                + cp.getTextColor().getRed()
                + ","
                + cp.getTextColor().getGreen()
                + ","
                + cp.getTextColor().getBlue()
                + ","
                + cp.getTextColor().getAlpha()
                + ")," +
                "new java.awt.Color("
                + cp.getBackgroundColor().getRed()
                + ","
                + cp.getBackgroundColor().getGreen()
                + ","
                + cp.getBackgroundColor().getBlue()
                + ","
                + cp.getBackgroundColor().getAlpha()
                + ")" +
                ")";
    }

    @Override
    public Object getValue() {
        return cpp.getSelectedValue();
    }

}
```

We now just need to link it all together by creating a new BeanInfo directly in NetBeans:

![Tutorial PNG](./media/tut2.png)

## Fourth step: Button hover logic

Now all there's left to do is code the logic that lets our custom Button switch between the two states (`hover` and `non-hover`), and also switch the text and background colors according to the info stored in each ColorProperty:

```java
    private void updateColors() {
        setOpaque(true);

        ColorProperty usedColor;
        if (isHover) usedColor = hoverColor;
        else usedColor = color;

        setForeground(usedColor.getTextColor());
        setBackground(usedColor.getBackgroundColor());
        setBorderPainted(false);
    }
```

That's it! We correctly implemented our custom button.

![Usage GIF](./media/usage1.gif)